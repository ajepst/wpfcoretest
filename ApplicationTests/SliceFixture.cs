﻿using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using TestCoreApplication;

namespace ApplicationTests
{
    static class SliceFixture {

        private static readonly IServiceScopeFactory _scopeFactory;
        private static readonly ServiceCollection _baseServices;


        static SliceFixture()
        {
            var startup = new App();
            _baseServices = new ServiceCollection(); 

            startup.ConfigureServices(_baseServices);

            var provider = _baseServices.BuildServiceProvider();
            _scopeFactory = provider.GetService<IServiceScopeFactory>();

            var scope = _scopeFactory.CreateScope();

        }

        public static Task<TResponse> SendAsync<TResponse>(IRequest<TResponse> request)
        {
            return ExecuteScopeAsync(sp =>
            {
                var mediator = sp.GetService<IMediator>();

                return mediator.Send(request);
            });
        }

        public static async Task<T> ExecuteScopeAsync<T>(Func<IServiceProvider, Task<T>> action)
        {
            using var scope = _scopeFactory.CreateScope();
            return await ExecuteScopeAsync(scope, action);
        }

        private static async Task<T> ExecuteScopeAsync<T>(IServiceScope scope, Func<IServiceProvider, Task<T>> action)
        {
            var result = await action(scope.ServiceProvider).ConfigureAwait(false);
            return result;
        }
    }
}