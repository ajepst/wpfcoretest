﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Shouldly;
using TestCoreApplication;

namespace ApplicationTests
{
    public class SimpleHandlerTests
    {
        public async Task Should_get_handler_result()
        {
            var result = await SliceFixture.SendAsync(new FakeRequest());
           // var handler = new TestHandler();
           // var result = await handler.Handle(new FakeRequest(), CancellationToken.None);
            result.ShouldBe("Hi from the Handler!");
        }
    }
}
