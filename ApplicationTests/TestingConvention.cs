﻿using Fixie;

namespace ApplicationTests
{
    public class TestingConvention : Discovery, Execution
    {
        public TestingConvention()
        {
            Methods.Where(m => m.Name != "SetUp" && m.Name != "TearDown");
        }

        public void Execute(TestClass testClass)
        {
            testClass.RunCases(@case =>
            {
                var instance = testClass.Construct();

                SetUp(instance);

                @case.Execute(instance);

                TearDown(instance);
            });
        }

        static void SetUp(object instance)
        {
            instance.GetType().GetMethod("SetUp")?.Execute(instance);
        }

        static void TearDown(object instance)
        {
            instance.GetType().GetMethod("TearDown")?.Execute(instance);
        }
    }
}